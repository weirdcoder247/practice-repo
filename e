[1mdiff --git a/02_codes/download__stockdata.py b/02_codes/download__stockdata.py[m
[1mindex 59e3db1..bdd29df 100644[m
[1m--- a/02_codes/download__stockdata.py[m
[1m+++ b/02_codes/download__stockdata.py[m
[36m@@ -1,16 +1,17 @@[m
[32m+[m[32m#############################[m
[32m+[m[32m###### Import packages ######[m
[32m+[m[32m#############################[m
 import numpy as np[m
 import pandas as pd[m
 import os[m
[31m-[m
 #Data Source[m
 import yfinance as yf[m
[31m-[m
 #Data viz[m
 import plotly.graph_objs as go[m
 import plotly[m
 [m
 #Interval required 5 minutes[m
[31m-data = yf.download(tickers='UBER', period='5d', interval='5m')[m
[32m+[m[32mdata = yf.download(tickers='UBER', period='5Y', interval='5m')[m
 [m
 #declare figure[m
 fig = go.Figure()[m
